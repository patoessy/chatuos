/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ws;
function connect() {
    if ('WebSocket' in window) {
        alert("Supported");
        var host = window.location.host;
        console.log(host);
        var wsURL = "ws://" + host + "/chatuos/hello";
        //console.log(wsURL);
        ws = new WebSocket(wsURL);
        ws.onopen=function(){
            console.log("Connection established");
        };
        ws.onmessage = function (evt) {
            var msg_received = evt.data;
            console.log("Received ");
            $('#mess').prepend("<p class='primary'> Receieve email: " + evt.data + "</p>");
        };
        ws.onclose = function(){
            console.log("Connection terminated");
        };
        ws.onerror = function(evt){
            console.log("An error occured" + evt);
        };
        return ws;
    } else {
        console.log("Not supported");
        return false;
    }
}
function sendMessage(sms) {
    ws.send("From client : " + sms);
    console.log("Message to sent "+sms);
}
$(function () {

    $('#wsRelay').click(function () {
        var sms = $('.userMessage').val();
        alert("send" + sms);
        sendMessage(sms);
    });
});
connect();
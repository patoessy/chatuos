/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ssmb.query;

/**
 *
 * @author patrick
 */
import com.ssmb.entity.Users;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DataQuery {
    EntityManagerFactory emf;
    EntityManager em;   
    public DataQuery(){
        emf = Persistence.createEntityManagerFactory("chatuosPU");
        em = emf.createEntityManager();
        em.getTransaction().begin();
    }
    public boolean loginControl(String username, String password){
        try {
            System.out.println("class DataQuery:  "+username + " + " +password);
            Users user =(Users) em.createNamedQuery("Users.control",Users.class).
                    setParameter("username", username).setParameter("password", password).getSingleResult();
            System.out.println("Users are");
            return user != null;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ssmb.controller;

import com.ssmb.query.DataQuery;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author patrick
 */
@Named("users")
@SessionScoped
public class LoginController implements Serializable {

    private String username;
    private String password;
    private final DataQuery query = new DataQuery();
    
    /**
     * <p>The method is used for login</p>
    *@return Returns the next page after successful login
    */
    public String loginController() {
        System.out.println(username + " + " + password);
        if (query.loginControl(username, password)) {
            HttpSession session = SessionUtils.getSession();
            session.setAttribute("username", username);
            return "home.xhtml?faces-redirect=true";
        }
        RequestContext.getCurrentInstance().update("growl");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Username or password is incorrect"));
        return "";
    }
    
    /**
     * @return returns the login page
    */
    public String logout(){
        HttpSession session = SessionUtils.getSession();
        session.invalidate();
        return "index.xhtml";
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ssmb.rest;

import javafx.geometry.Side;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 * @author patpat
 */
// www.localhost:8080/chatuos/resources/hi/say
@Path("hi")
public class HelloRest {
    @Path("say")
    @GET
    @Produces("text/plain")
    public String sayHello(){
        return "Hello rest";
    }
    
    @Path("user")
    @GET
    @Produces("application/xml")
    public SimpleUser getUser(){
        return new SimpleUser("Pato", "pashsdfg");
    }
}
